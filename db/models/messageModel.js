const mongoose = require("mongoose");

const ChatSchema = new mongoose.Schema({
  sender: String,
  message: String,
  createdAt: { type: Date, default: Date.now }
});
const Chat = mongoose.model('message', ChatSchema);
module.exports = Chat;
