const koa = require("koa");
const koaBody = require("koa-bodyparser");
const koaStatic = require("koa-static");
const views = require("koa-views");
const http = require("http");
const socketio = require("socket.io");
const path = require("path");
const dotenv = require("dotenv");
const Router = require("koa-router");
const router = new Router();
const connectDB = require("./db");
const Massage = require("./db/models/messageModel");

connectDB();
const app = new koa();
dotenv.config();

app.use(koaBody());

// set up path and static file
app.use(
  views(path.join(__dirname, "views"), {
    extension: "ejs",
  })
);
app.use(koaStatic(__dirname + "/public"));

// routes
app.use(async (ctx) => {
  await ctx.render("index");
});
app.use(router.routes()).use(router.allowedMethods());

// socket io
const server = http.createServer(app.callback());
const io = socketio(server);

io.on("connection", async (socket) => {
  const limitMessage = 10;
  socket.username = "Anonnymous";

  // Get the last 20 messages
  let current_message = await Massage.find()
    .sort({ createdAt: -1 })
    .limit(limitMessage);
  current_message.reverse();
  io.sockets.emit("current_message", {
    message: current_message,
    skip: 1,
  });

  // set username
  socket.on("login", ({ username }) => {
    socket.username = username;
  });

  // refresh chat after change username
  socket.on("refresh", async () => {
    let current_message = await Massage.find()
      .sort({ _id: -1 })
      .limit(limitMessage);

    current_message.reverse();
    io.sockets.emit("current_message", {
      message: current_message,
    });
  });

  // handle the new message event
  socket.on("new_message", async ({ username, message }) => {
    const sender = username;
    // save to messages collection
    const addMessage = new Massage({
      sender,
      message,
    });
    await addMessage.save();

    // send latest message to chat
    io.sockets.emit("receive_message", {
      message: addMessage.message,
      username: username,
      createdAt: addMessage.createdAt,
    });
  });

  // get more messages 
  socket.on("get_more_message", async ({ skip }) => {
    const page = parseInt(skip) || 1;
    const messages = await Massage.find()
      .sort({ createdAt: -1 })
      .skip((page - 1) * limitMessage)
      .limit(limitMessage);

    if (messages.length) {
      io.sockets.emit("receive_more_message", {
        message: messages,
        skip: skip + 1,
      });
    }
  });
});

// run server
const port = process.env.PORT;
server.listen(port, () => {
  console.log(`Server Koa.js running at port ${port}`);
});
