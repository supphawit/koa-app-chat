const socket = io.connect("http://localhost:3000/");

socket.on("connect", function () {
  console.log("เชื่อมต่อแล้ว");
});

// get user from local storeage
let getUser = localStorage.getItem("username");
let displayUsername = document.getElementById("username");

if (!getUser) {
  let username = prompt("กรุณากรอกชื่อของคุณ:");
  while (!username) {
    username = prompt("กรุณากรอกชื่อของคุณ:");
  }
  getUser = username;
  displayUsername.innerHTML = getUser;
  localStorage.setItem("username", username);
  socket.emit("login", { username: getUser });
} else {
  displayUsername.innerHTML = getUser;
  socket.emit("login", { username: getUser });
}

// handle receive message from server
socket.on("receive_message", function ({ message, username, createdAt }) {
  if (getUser !== username) {
    var chatBox = document.getElementsByClassName("chat-box")[0];
    var messageElement = document.createElement("div");
    messageElement.className = "message left-message";
    messageElement.innerHTML = `
    <div class="left-message-box">
      <div class="message-username">
        ${username}
      </div>
      <div class="message-text">
        ${message}
      </div>
      <div class="message-time">
        ${convertToTime(createdAt)}
      </div>
    </div>
  `;
    chatBox.appendChild(messageElement);
    scrollToBottom();
  }
});

// init chat
let skipDefault;
socket.on("current_message", async ({ message, skip }) => {
  skipDefault = skip;
  const messageList = message;
  var chatBox = document.getElementsByClassName("chat-box")[0];
  chatBox.innerHTML = "";
  for (const msg of messageList) {
    const isUser = getUser === msg.sender ? "right" : "left";
    var messageElement = document.createElement("div");
    messageElement.className = `message ${isUser}-message`;
    messageElement.innerHTML = `
    <div class="${isUser}-message-box">
      <div class="message-username">
        ${msg.sender}
      </div>  
      <div class="message-text">
        ${msg.message}
      </div>
      <div class="message-time">
        ${convertToTime(msg.createdAt)}
      </div>
    </div>
  `;
    chatBox.appendChild(messageElement);
    scrollToBottom();
  }
});

// submit message to server
document
  .getElementById("chat-form")
  .addEventListener("submit", function (event) {
    event.preventDefault();
    var messageInput = document.getElementById("message-input");
    var message = messageInput.value.trim();

    if (message !== "") {
      var chatBox = document.getElementsByClassName("chat-box")[0];
      var messageElement = document.createElement("div");
      messageElement.className = "message right-message";
      messageElement.innerHTML = `
      <div class="right-message-box">
        <div class="message-username">
          ${getUser}
        </div>
        <div class="message-text">
          ${message}
        </div>
        <div class="message-time">
          ${getCurrentTime()}
        </div>
      </div>
    `;
      chatBox.appendChild(messageElement);
      socket.emit("new_message", { username: getUser, message: message });
      scrollToBottom();

      messageInput.value = "";
    }
  });

// get more message from server
var chatBox = document.getElementById("chat-box");
chatBox.addEventListener("scroll", function () {
  if (chatBox.scrollTop === 0) {
    skipDefault += 1;
    socket.emit("get_more_message", { skip: skipDefault });
  }
});

// receive more message from server
socket.on("receive_more_message", function ({ message, skip }) {
  for (const msg of message) {
    const isUser = getUser === msg.sender ? "right" : "left";
    var messageElement = document.createElement("div");
    messageElement.className = `message ${isUser}-message`;
    messageElement.innerHTML = `
    <div class="${isUser}-message-box">
      <div class="message-username">
        ${msg.sender}
      </div>  
      <div class="message-text">
        ${msg.message}
      </div>
      <div class="message-time">
        ${convertToTime(msg.createdAt)}
      </div>
    </div>
  `;
    chatBox.prepend(messageElement);
  }
});

const changeUsername = () => {
  let username = prompt("กรุณากรอกชื่อของคุณ:");
  while (!username) {
    username = prompt("กรุณากรอกชื่อของคุณ:");
  }
  getUser = username;
  displayUsername.innerHTML = getUser;
  localStorage.setItem("username", getUser);

  socket.emit("login", { username: getUser });
  socket.emit("refresh", { isRefresh: true });
};

const scrollToBottom = () => {
  let chatBox = document.getElementsByClassName("chat-box")[0];
  chatBox.scrollTop = chatBox.scrollHeight;
};

const getCurrentTime = () => {
  var date = new Date();
  var hours = date.getHours();
  var minutes = date.getMinutes();
  var ampm = hours >= 12 ? "PM" : "AM";
  hours = hours % 12;
  hours = hours ? hours : 12;
  minutes = minutes < 10 ? "0" + minutes : minutes;
  var currentTime = hours + ":" + minutes + " " + ampm;
  return currentTime;
};

const convertToTime = (timestamp) => {
  const date = new Date(timestamp);

  const hours = date.getHours().toString().padStart(2, "0");
  const minutes = date.getMinutes().toString().padStart(2, "0");

  const time = `${hours}:${minutes}`;
  return time;
};
