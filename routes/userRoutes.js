const Router = require("koa-router");
const router = new Router({ prefix: "/chats" });

const { chats } = require("../data/data");

router.get("/", (ctx, next) => {
  ctx.body = chats;
  next();
});

router.get("/:id", (ctx, next) => {
  const findChat = chats.find((e) => e._id === ctx.params.id);
  ctx.body = findChat;
  next();
});

module.exports = router;
