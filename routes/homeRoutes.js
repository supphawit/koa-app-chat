const Router = require("koa-router");
const views = require("koa-view");
const router = new Router({ prefix: "/" });

// กำหนดเส้นทางสำหรับหน้าหลัก
router.get("/", async (ctx) => {
  await ctx.render("index"); // ชื่อไฟล์หน้า HTML
});

// router.get("/", (ctx) => {
//   ctx.r = chats;
//   next();
// });

module.exports = router;
